#!/bin/sh
# Prepare to send a folder (login.forum.downingjcr.co.uk.temp) to the Downing JCR Server
set -e

mkdir login.forum.downingjcr.co.uk.temp/

# Copy necessary folders
cp -r dist/ login.forum.downingjcr.co.uk.temp/
cp -r public/ login.forum.downingjcr.co.uk.temp/
cp -r views/ login.forum.downingjcr.co.uk.temp/

# Copy necessary files
cp deploy.sh login.forum.downingjcr.co.uk.temp/
cp package.json login.forum.downingjcr.co.uk.temp/

(cd login.forum.downingjcr.co.uk.temp/ && ls -a)
