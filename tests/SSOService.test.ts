import { mockUsers, mockSSO } from './mockData';
import SSOService from '../src/services/SSOService';

const ssoService = new SSOService();

beforeAll(() => {
	process.env.SSO_SECRET = mockSSO.secret;
	delete process.env.LOG_PATH;
	delete process.env.SENTRY_DSN;
});

afterAll(() => {
	delete process.env.SSO_SECRET;
});

describe('Test the SSOService methods dealing with incoming payloads', () => {
	test('.validate() should successfully validate the payload if correct', () => {
		const valid = ssoService.validatePayload(mockSSO.valid.payload, mockSSO.valid.signature);
		expect(valid).toEqual(true);
	});

	test(".validate() should not validate the payload if signiture doesn't match", () => {
		expect(ssoService.validatePayload(mockSSO.valid.payload.substr(1), mockSSO.valid.signature)).toEqual(false);
		expect(ssoService.validatePayload(mockSSO.valid.payload, mockSSO.valid.signature.substr(1))).toEqual(false);
	});

	test('.getNonce() should extract that correct nonce', () => {
		const nonce = ssoService.getNonce(mockSSO.valid.payload);
		expect(nonce).toEqual(mockSSO.nonce);
	});
});

describe('Test the SSOService methods dealing with outgoing payloads', () => {
	test('.buildLoginString() should throw an error if required parameters are missing', () => {
		function buildWithIncompleteParams(): void {
			ssoService.buildLoginString(
				{
					nonce: mockSSO.nonce,
					email: mockUsers.me.email,
					external_id: mockUsers.me.crsid,
					admin: mockUsers.me.admin,
					moderator: mockUsers.me.moderator,
					roles: mockUsers.me.roles,
				},
				true
			);
		}

		expect(buildWithIncompleteParams).toThrowError(/^Missing required parameter/);
	});

	test('.buildLoginString() should throw an error if email is invalid', () => {
		function buildWithIncompleteParams(): void {
			ssoService.buildLoginString(
				{
					nonce: mockSSO.nonce,
					email: 'dow99@cam.uk',
					external_id: mockUsers.me.crsid,
					username: mockUsers.me.crsid,
					admin: mockUsers.me.admin,
					moderator: mockUsers.me.moderator,
					roles: mockUsers.me.roles,
				},
				true
			);
		}
		expect(buildWithIncompleteParams).toThrowError(/^"dow99@cam.uk" is not a valid value for the parameter "email"$/);
	});

	test('.buildLoginString() should throw an error if the username does not match the external_id', () => {
		function buildWithIncompleteParams(): void {
			ssoService.buildLoginString(
				{
					nonce: mockSSO.nonce,
					email: mockUsers.me.email,
					external_id: mockUsers.me.crsid,
					username: mockUsers.me.crsid.substr(1),
					admin: mockUsers.me.admin,
					moderator: mockUsers.me.moderator,
					roles: mockUsers.me.roles,
				},
				true
			);
		}
		expect(buildWithIncompleteParams).toThrowError(/^External id "dow99" does not match username "ow99"$/);
	});

	test('.buildLoginString() should throw an error if the email does not match the external_id', () => {
		function buildWithIncompleteParams(): void {
			ssoService.buildLoginString(
				{
					nonce: mockSSO.nonce,
					email: mockUsers.me.email.substr(1),
					external_id: mockUsers.me.crsid,
					username: mockUsers.me.crsid,
					admin: mockUsers.me.admin,
					moderator: mockUsers.me.moderator,
					roles: mockUsers.me.roles,
				},
				true
			);
		}
		expect(buildWithIncompleteParams).toThrowError(
			/^External id "dow99" does not match start of email "ow99@cam.ac.uk"$/
		);
	});
});
