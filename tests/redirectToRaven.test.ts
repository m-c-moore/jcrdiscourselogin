/*
@jest-environment jsdom
 */

import { mockHTML, mockQueries, mockSSO } from './mockData';
import { checkXHTTPResponse } from '../src/frontend/redirectToRaven';
import * as render from '../src/frontend/renderValidationResults';

const mockOpen = jest.fn();
const mockSend = jest.fn();
const mockLoad = jest.fn();
const mockWindowReplace = jest.fn();

const mockXHR = (): { open: jest.Mock; onload: jest.Mock; send: jest.Mock } => ({
	open: mockOpen,
	onload: mockLoad,
	send: mockSend,
});

describe('Test the the redirectToRaven script', () => {
	beforeEach(() => {
		document.title = mockHTML.original.title;
		document.body.innerHTML = mockHTML.original.innerHTML;

		(global as any).XMLHttpRequest = jest.fn().mockImplementation(mockXHR);
		Object.defineProperty(window, 'location', {
			writable: true,
			value: {
				href: `http://url/?${mockQueries.login.valid}`,
				replace: mockWindowReplace,
			},
		});
	});

	afterEach(() => {
		jest.clearAllMocks();
		sessionStorage.clear();
	});

	test('validateSSO() should be called and make the correct requests on window load', () => {
		(global as any).dispatchEvent(new Event('load'));
		expect(mockOpen).toBeCalledWith('GET', `/validate/?${mockQueries.login.valid}`, true);
		expect(mockSend).toHaveBeenCalledTimes(1);
	});

	test('checkXHTTPResponse() should redirect to /login and store the nonce on status 200', () => {
		const mockXHTTP = {
			status: 200,
			response: JSON.stringify({
				nonce: mockSSO.nonce,
			}),
		};
		checkXHTTPResponse(mockXHTTP as XMLHttpRequest);
		expect(mockWindowReplace).toBeCalledWith('/login');
		expect(sessionStorage.getItem('discourse_nonce')).toEqual(mockSSO.nonce);
	});

	test('checkXHTTPResponse() should call renderValidationFailure() on any other status', () => {
		const mockXHTTP = {
			status: 400,
			response: JSON.stringify({
				title: 'failed',
			}),
		};
		const spyRender = jest.spyOn(render, 'renderValidationFailure');

		checkXHTTPResponse(mockXHTTP as XMLHttpRequest);
		expect(mockWindowReplace).toBeCalledTimes(0);
		expect(spyRender).toBeCalledWith(mockXHTTP.status, JSON.parse(mockXHTTP.response).title);
		expect(sessionStorage.getItem('discourse_nonce')).toEqual(null);
	});
});
