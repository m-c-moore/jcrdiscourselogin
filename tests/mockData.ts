/* eslint max-len: 0 */
import path from 'path';

const nonce = 'mock_base64_nonce';

export const mockUsers = {
	me: {
		name: 'Me Myself',
		crsid: 'dow99',
		admin: undefined,
		moderator: 'false',
		email: 'dow99@cam.ac.uk',
		roles: [''],
	},
	anAdmin: {
		name: 'Mrs Admin',
		crsid: 'admin01',
		admin: 'true',
		moderator: 'true',
		email: 'admin01@cam.ac.uk',
		groups: 'group_of_admins,group_of_mods',
		roles: 'Admin;Also a Mod',
	},
	aMod: {
		name: 'Mr Mod',
		crsid: 'mod02',
		admin: 'false',
		moderator: 'true',
		email: 'mod02@cam.ac.uk',
		addGroups: 'group_of_mods',
		roles: 'Just a Mod',
	},
	comms: {
		name: 'Dr Comms',
		crsid: 'comms03',
		admin: 'true',
		moderator: 'true',
		email: 'comms03@cam.ac.uk',
		jcrEmail: 'comms@jcr.ac.uk',
		jcrName: 'JCR Comms',
		jcrUsername: 'jcr_username',
		roles: [''],
	},
	fake: {
		crsid: 'not99',
	},
};

export const mockAuth = {
	authorisedRequest: {
		me: {
			query: { nonce },
			user: { crsid: mockUsers.me.crsid },
			isAuthenticated: (): boolean => true,
		},
		anAdmin: {
			query: { nonce },
			user: { crsid: mockUsers.anAdmin.crsid },
			isAuthenticated: (): boolean => true,
		},
		aMod: {
			query: { nonce },
			user: { crsid: mockUsers.aMod.crsid },
			isAuthenticated: (): boolean => true,
		},
		comms: {
			query: { nonce },
			user: { crsid: mockUsers.comms.crsid },
			isAuthenticated: (): boolean => true,
		},
	},
	unauthorisedRequest: {
		query: { nonce },
		user: { crsid: mockUsers.me.crsid },
		isAuthenticated: (): boolean => false,
	},
	forbiddenRequest: {
		query: { nonce },
		user: { crsid: mockUsers.fake.crsid },
		isAuthenticated: (): boolean => true,
	},
	invalidRequest: {
		query: {},
		user: { crsid: mockUsers.me.crsid },
		isAuthenticated: (): boolean => true,
	},
	csvPath: path.join(__dirname, 'mockStudents.csv'),
};

export const mockSSO = {
	secret: 'mock_sso_secret',
	valid: {
		payload: 'bm9uY2U9bW9ja19iYXNlNjRfbm9uY2U%3D',
		signature: 'e7ce6877f00433051fe2e9f143f038a2a22f6add29b167d4ad2bf79bedad033d',
	},
	invalid: {
		payload: 'm9uY2U9bW9ja19iYXNlNjRfbm9uY2U%3D',
		signature: '6d154cb6d9c55e42ea247b71123e3f03a4ac79a144d5a3334ce20c4c7560667a',
	},
	redirection: {
		me: {
			payload:
				'bm9uY2U9bW9ja19iYXNlNjRfbm9uY2UmZW1haWw9ZG93OTklNDBjYW0uYWMudWsmZXh0ZXJuYWxfaWQ9ZG93OTkmdXNlcm5hbWU9ZG93OTkmbmFtZT1NZSUyME15c2VsZiZtb2RlcmF0b3I9ZmFsc2UmY3VzdG9tLnVzZXJfZmllbGRfMT0%3D',
			signature: '400a8008611788da465142ed01d02dc7cfdaa9aefe2b02a04ac42ec3d0407baf',
		},
		mrsAdmin: {
			payload:
				'bm9uY2U9bW9ja19iYXNlNjRfbm9uY2UmZW1haWw9YWRtaW4wMSU0MGNhbS5hYy51ayZleHRlcm5hbF9pZD1hZG1pbjAxJnVzZXJuYW1lPWFkbWluMDEmbmFtZT1NcnMlMjBBZG1pbiZhZG1pbj10cnVlJm1vZGVyYXRvcj10cnVlJmFkZF9ncm91cHM9Z3JvdXBfb2ZfYWRtaW5zJTJDZ3JvdXBfb2ZfbW9kcyZjdXN0b20udXNlcl9maWVsZF8xPUFuJTIwQWRtaW4lMkMlMjBBbHNvJTIwYSUyME1vZA%3D%3D',
			signature: 'a52e457c6c8ea24700d070d12703303bbe37200d5d032544730d7f926d9b99f1',
		},
		mrMod: {
			payload:
				'bm9uY2U9bW9ja19iYXNlNjRfbm9uY2UmZW1haWw9bW9kMDIlNDBjYW0uYWMudWsmZXh0ZXJuYWxfaWQ9bW9kMDImdXNlcm5hbWU9bW9kMDImbmFtZT1NciUyME1vZCZhZG1pbj1mYWxzZSZtb2RlcmF0b3I9dHJ1ZSZhZGRfZ3JvdXBzPWdyb3VwX29mX21vZHMmY3VzdG9tLnVzZXJfZmllbGRfMT1KdXN0JTIwYSUyME1vZA%3D%3D',
			signature: 'e80752633edd4c76de777d37a6dcdad5566ed6039ba8485508ed4c47a9bed92e',
		},
		comms: {
			payload:
				'bm9uY2U9bW9ja19iYXNlNjRfbm9uY2UmZW1haWw9Y29tbXMlNDBqY3IuYWMudWsmZXh0ZXJuYWxfaWQ9amNyX3VzZXJuYW1lJnVzZXJuYW1lPWpjcl91c2VybmFtZSZuYW1lPUpDUiUyMENvbW1zJmFkbWluPXRydWUmbW9kZXJhdG9yPXRydWU%3D',
			signature: '2eb6e8c6e6d33e9c3811cadb6ac24f3014aa0295d5821efb8c6dfa7b36c34cc8',
		},
	},
	nonce,
};

export const mockQueries = {
	login: {
		valid: `sso=${mockSSO.valid.payload}&sig=${mockSSO.valid.signature}`,
		invalidPayload: `sso=${mockSSO.invalid.payload}&sig=${mockSSO.invalid.signature}`,
		invalidSignature: `sso=${mockSSO.valid.payload}&sig=${mockSSO.valid.signature}X`,
	},
	identified: {
		valid: `nonce=${nonce}&crsid=${mockUsers.me.crsid}`,
		invalid: `crsid=${mockUsers.me.crsid}`,
	},
	redirection: {
		me: `sso=${mockSSO.redirection.me.payload}&sig=${mockSSO.redirection.me.signature}`,
		anAdmin: `sso=${mockSSO.redirection.mrsAdmin.payload}&sig=${mockSSO.redirection.mrsAdmin.signature}`,
		aMod: `sso=${mockSSO.redirection.mrMod.payload}&sig=${mockSSO.redirection.mrMod.signature}`,
		comms: `sso=${mockSSO.redirection.comms.payload}&sig=${mockSSO.redirection.comms.signature}`,
	},
};

export const mockHTML = {
	original: {
		title: 'oldTitle',
		innerHTML:
			'<before></before><div class="container"><div id="card-placeholder" class="d-flex"><h5>Redirecting...</h5></div></div><after></after>',
	},
	card: {
		title: 'newTitle',
		subtitle: 'newSubtitle',
		message: 'newMessage',
		contact: 'newContact',
	},
	failure: {
		status: 400,
		title: 'SSO payload not found',
		subtitle: 'Sorry, we could not find an SSO payload in your request.',
		contact: 'If the problem persists',
	},
	noNonce: {
		title: 'New session who dis?',
		subtitle: "Sorry, we couldn't verify your login session.",
		contact: "If that isn't the case and the problem persists",
	},
	idSuccess: {
		title: `Welcome, ${mockUsers.me.name}!`,
		message: 'You will shortly be redirected',
	},
	idFailure: {
		status: 403,
		title: 'Not a valid user',
		subtitle: `Sorry ${mockUsers.fake.crsid}, our records show`,
		contact: 'If you believe this is a mistake',
	},
};

export const mockVotes = {
	secret: 'mock_vote_secret',
	key: '87187f8cd8400910d5a1b1d7bc9f2c0c6ab6fe3a10874f192cb0dbe98dfe53af',
};
