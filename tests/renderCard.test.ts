/*
@jest-environment jsdom
 */

import { mockHTML, mockUsers } from './mockData';
import renderCard from '../src/frontend/renderCard';
import { renderValidationFailure } from '../src/frontend/renderValidationResults';
import {
	renderNoNonce,
	renderIdentificationSuccess,
	renderIdentificationFailure,
} from '../src/frontend/renderIdentificationResults';

describe('Test renderCard()', () => {
	beforeEach(() => {
		document.title = mockHTML.original.title;
		document.body.innerHTML = mockHTML.original.innerHTML;
	});

	test('renderCard() should correctly modify the window title and the innerHTML of the card-placeholder div (and nothing else)', () => {
		renderCard(mockHTML.card.title, mockHTML.card.subtitle, mockHTML.card.message, mockHTML.card.contact);

		let cardPattern = '.*(<div class="card">).*';
		Object.keys(mockHTML.card).forEach((key: string): void => {
			cardPattern += `(${mockHTML.card[key]}).*`;
		});

		const htmlPattern = /^(<before><\/before>).*(<after><\/after>)$/;

		expect(document.title).toMatch(mockHTML.card.title);
		expect(document.body.innerHTML).toMatch(new RegExp(cardPattern));
		expect(document.body.innerHTML).toMatch(htmlPattern);
	});
});

describe('Test the rendering functions used by the redirectToRaven script', () => {
	beforeEach(() => {
		document.title = mockHTML.original.title;
		document.body.innerHTML = mockHTML.original.innerHTML;
	});

	test('renderValidationFailure() should correctly modify the window title and the innerHTML of the card-placeholder div', () => {
		renderValidationFailure(mockHTML.failure.status, mockHTML.failure.title);

		let cardPattern = '.*(<div class="card">).*';
		cardPattern += `(${mockHTML.failure.status}: ${mockHTML.failure.title}).*`;
		cardPattern += `(${mockHTML.failure.subtitle}).*`;
		cardPattern += `(${mockHTML.failure.contact}).*`;

		expect(document.title).toMatch(mockHTML.failure.title);
		expect(document.body.innerHTML).toMatch(new RegExp(cardPattern));
	});
});

describe('Test rendering functions used by the redirectToDiscourse script', () => {
	beforeEach(() => {
		document.title = mockHTML.original.title;
		document.body.innerHTML = mockHTML.original.innerHTML;
	});

	test('renderNoNonce() should correctly modify the window title and the innerHTML of the card-placeholder div', () => {
		renderNoNonce();

		let cardPattern = '.*(<div class="card">).*';
		Object.keys(mockHTML.noNonce).forEach((key: string): void => {
			cardPattern += `(${mockHTML.noNonce[key]}).*`;
		});

		expect(document.title).toMatch(mockHTML.noNonce.title);
		expect(document.body.innerHTML).toMatch(new RegExp(cardPattern));
	});

	test('renderIdentificationSuccess() should correctly modify the window title and the innerHTML of the card-placeholder div', () => {
		renderIdentificationSuccess(mockUsers.me.name);

		let cardPattern = '.*(<div class="card">).*';
		Object.keys(mockHTML.idSuccess).forEach((key: string): void => {
			cardPattern += `(${mockHTML.idSuccess[key]}).*`;
		});

		expect(document.title).toMatch(mockHTML.idSuccess.title);
		expect(document.body.innerHTML).toMatch(new RegExp(cardPattern));
	});

	test('renderIdentificationFailure() should correctly modify the window title and the innerHTML of the card-placeholder div', () => {
		renderIdentificationFailure(mockHTML.idFailure.status, mockHTML.idFailure.title, mockUsers.fake.crsid);

		let cardPattern = '.*(<div class="card">).*';
		cardPattern += `(${mockHTML.idFailure.status}: ${mockHTML.idFailure.title}).*`;
		cardPattern += `(${mockHTML.idFailure.subtitle}).*`;
		cardPattern += `(${mockHTML.idFailure.contact}).*`;

		expect(document.title).toMatch(mockHTML.idFailure.title);
		expect(document.body.innerHTML).toMatch(new RegExp(cardPattern));
	});
});
