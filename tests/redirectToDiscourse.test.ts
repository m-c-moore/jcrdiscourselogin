/*
@jest-environment jsdom
 */

import { mockHTML, mockQueries, mockSSO, mockUsers } from './mockData';
import { checkXHTTPResponse } from '../src/frontend/redirectToDiscourse';
import * as render from '../src/frontend/renderIdentificationResults';

const mockOpen = jest.fn();
const mockSend = jest.fn();
const mockLoad = jest.fn();
const mockWindowReplace = jest.fn();

const mockXHR = (): { open: jest.Mock; onload: jest.Mock; send: jest.Mock } => ({
	open: mockOpen,
	onload: mockLoad,
	send: mockSend,
});

describe('Test the the redirectToDiscourse script', () => {
	beforeEach(() => {
		document.title = mockHTML.original.title;
		document.body.innerHTML = mockHTML.original.innerHTML;

		(global as any).XMLHttpRequest = jest.fn().mockImplementation(mockXHR);
		Object.defineProperty(window, 'location', {
			writable: true,
			value: {
				replace: mockWindowReplace,
			},
		});
	});

	afterEach(() => {
		jest.clearAllMocks();
		sessionStorage.clear();
	});

	test('identifyUser() should be called and call renderNoNonce() if there is no nonce', () => {
		const spyRender = jest.spyOn(render, 'renderNoNonce');
		(global as any).dispatchEvent(new Event('load'));

		expect(spyRender).toHaveBeenCalledTimes(1);
	});

	test('identifyUser() should be called and make the correct requests on window load (if there is a nonce)', () => {
		sessionStorage.setItem('discourse_nonce', mockSSO.nonce);

		(global as any).dispatchEvent(new Event('load'));
		expect(mockOpen).toBeCalledWith('GET', `/identify/?nonce=${mockSSO.nonce}`, true);
		expect(mockSend).toHaveBeenCalledTimes(1);
	});

	test('checkXHTTPResponse() should render success and redirect to /discourse on status 200', () => {
		const mockXHTTP = {
			status: 200,
			response: JSON.stringify({
				name: mockUsers.me.name,
				redirectQuery: mockQueries.redirection.me,
			}),
		};
		const spyRender = jest.spyOn(render, 'renderIdentificationSuccess');
		const discourseSessionURL = 'http://forum.downingjcr.co.uk/session/sso_login';

		checkXHTTPResponse(mockXHTTP as XMLHttpRequest);
		expect(mockWindowReplace).toBeCalledWith(`${discourseSessionURL}/?${JSON.parse(mockXHTTP.response).redirectQuery}`);
		expect(spyRender).toBeCalledWith(mockUsers.me.name);
	});

	test('checkXHTTPResponse() should call renderIdentificationFailure() on any other status', () => {
		const mockXHTTP = {
			status: 403,
			response: JSON.stringify({
				title: 'failed',
				crsid: mockUsers.me.crsid,
			}),
		};
		const spyRender = jest.spyOn(render, 'renderIdentificationFailure');

		checkXHTTPResponse(mockXHTTP as XMLHttpRequest);
		expect(mockWindowReplace).toBeCalledTimes(0);
		expect(spyRender).toBeCalledWith(
			mockXHTTP.status,
			JSON.parse(mockXHTTP.response).title,
			JSON.parse(mockXHTTP.response).crsid
		);
	});
});
