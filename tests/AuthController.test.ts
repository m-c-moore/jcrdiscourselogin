import { mockUsers, mockAuth, mockSSO, mockQueries, mockVotes } from './mockData';
import AuthController, { AuthResponse } from '../src/controllers/AuthController';

const authController = new AuthController();

beforeAll(() => {
	process.env.SSO_SECRET = mockSSO.secret;
	process.env.CSV_PATH = mockAuth.csvPath;
	process.env.JCR_COMMS_CRSID = mockUsers.comms.crsid;
	process.env.JCR_COMMS_EMAIL = mockUsers.comms.jcrEmail;
	process.env.JCR_COMMS_USERNAME = mockUsers.comms.jcrUsername;
	delete process.env.LOCKED;
	delete process.env.LOG_PATH;
	delete process.env.SENTRY_DSN;
});

afterAll(() => {
	delete process.env.SSO_SECRET;
	delete process.env.CSV_PATH;
	delete process.env.JCR_COMMS_CRSID;
	delete process.env.JCR_COMMS_EMAIL;
	delete process.env.JCR_COMMS_USERNAME;
});

describe('Test the identify() method of AuthController with good requests', () => {
	test('Authenticated Downing student should receive status 200 and query string', async () => {
		const mockResponse = {
			status: jest.fn((statusCode: number): number => statusCode),
			send: jest.fn((authResponse: AuthResponse): AuthResponse => authResponse),
		};

		const expectedResponse: AuthResponse = {
			title: 'Successfully generated query string',
			name: mockUsers.me.name,
			redirectQuery: mockQueries.redirection.me,
		};

		await authController.identify(mockAuth.authorisedRequest.me, mockResponse);

		expect(mockResponse.status).toHaveBeenCalledWith(200);
		expect(mockResponse.status).toHaveBeenCalledTimes(1);
		expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
	});

	test('Authenticated admin should receive status 200 and query string', async () => {
		const mockResponse = {
			status: jest.fn((statusCode: number): number => statusCode),
			send: jest.fn((authResponse: AuthResponse): AuthResponse => authResponse),
		};

		const expectedResponse: AuthResponse = {
			title: 'Successfully generated query string',
			name: mockUsers.anAdmin.name,
			redirectQuery: mockQueries.redirection.anAdmin,
		};

		await authController.identify(mockAuth.authorisedRequest.anAdmin, mockResponse);

		expect(mockResponse.status).toHaveBeenCalledWith(200);
		expect(mockResponse.status).toHaveBeenCalledTimes(1);
		expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
	});

	test('Authenticated moderator should receive status 200 and query string', async () => {
		const mockResponse = {
			status: jest.fn((statusCode: number): number => statusCode),
			send: jest.fn((authResponse: AuthResponse): AuthResponse => authResponse),
		};

		const expectedResponse: AuthResponse = {
			title: 'Successfully generated query string',
			name: mockUsers.aMod.name,
			redirectQuery: mockQueries.redirection.aMod,
		};

		await authController.identify(mockAuth.authorisedRequest.aMod, mockResponse);

		expect(mockResponse.status).toHaveBeenCalledWith(200);
		expect(mockResponse.status).toHaveBeenCalledTimes(1);
		expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
	});

	test('Authenticated comms user should receive status 200 and query string', async () => {
		const mockResponse = {
			status: jest.fn((statusCode: number): number => statusCode),
			send: jest.fn((authResponse: AuthResponse): AuthResponse => authResponse),
		};

		const expectedResponse: AuthResponse = {
			title: 'Successfully generated query string',
			name: `${mockUsers.comms.crsid} (as ${mockUsers.comms.jcrUsername})`,
			redirectQuery: mockQueries.redirection.comms,
		};

		await authController.identify(mockAuth.authorisedRequest.comms, mockResponse);

		expect(mockResponse.status).toHaveBeenCalledWith(200);
		expect(mockResponse.status).toHaveBeenCalledTimes(1);
		expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
	});

	test('Authenticated Downing student should receive status 423 and vote key if locked', async () => {
		const mockResponse = {
			status: jest.fn((statusCode: number): number => statusCode),
			send: jest.fn((authResponse: AuthResponse): AuthResponse => authResponse),
		};

		const expectedResponse: AuthResponse = {
			title: 'Locked',
			crsid: mockUsers.me.crsid,
			voteKey: mockVotes.key,
		};

		process.env.LOCKED = 'true';
		process.env.VOTE_SECRET = mockVotes.secret;
		await authController.identify(mockAuth.authorisedRequest.me, mockResponse);
		delete process.env.LOCKED;

		expect(mockResponse.status).toHaveBeenCalledWith(423);
		expect(mockResponse.status).toHaveBeenCalledTimes(1);
		expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
	});
});

describe('Test the identify() method of AuthController with bad requests', () => {
	test('Non-authenticated Downing student should receive status 401 and "Unauthorised"', async () => {
		const mockResponse = {
			status: jest.fn((statusCode: number): number => statusCode),
			send: jest.fn((authResponse: AuthResponse): AuthResponse => authResponse),
		};

		const expectedResponse: AuthResponse = {
			title: 'Unauthorised',
		};

		await authController.identify(mockAuth.unauthorisedRequest, mockResponse);

		expect(mockResponse.status).toHaveBeenCalledWith(401);
		expect(mockResponse.status).toHaveBeenCalledTimes(1);
		expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
	});

	test('Authenticated non-Downing student should receive status 403 and "Forbidden"', async () => {
		const mockResponse = {
			status: jest.fn((statusCode: number): number => statusCode),
			send: jest.fn((authResponse: AuthResponse): AuthResponse => authResponse),
		};

		const expectedResponse: AuthResponse = {
			title: 'Forbidden',
			crsid: mockUsers.fake.crsid,
		};

		await authController.identify(mockAuth.forbiddenRequest, mockResponse);

		expect(mockResponse.status).toHaveBeenCalledWith(403);
		expect(mockResponse.status).toHaveBeenCalledTimes(1);
		expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
	});

	test('Authenticated Downing student without nonce should receive status 500 and error message', async () => {
		const mockResponse = {
			status: jest.fn((statusCode: number): number => statusCode),
			send: jest.fn((authResponse: AuthResponse): AuthResponse => authResponse),
		};

		const expectedResponse: AuthResponse = {
			title: 'Missing required parameter "nonce"',
			crsid: mockUsers.me.crsid,
		};

		await authController.identify(mockAuth.invalidRequest, mockResponse);

		expect(mockResponse.status).toHaveBeenCalledWith(500);
		expect(mockResponse.status).toHaveBeenCalledTimes(1);
		expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
	});

	test("Request made when SSO_SECRET doesn't exist should receive status 500 and error message", async () => {
		delete process.env.SSO_SECRET;
		const mockResponse = {
			status: jest.fn((statusCode: number): number => statusCode),
			send: jest.fn((authResponse: AuthResponse): AuthResponse => authResponse),
		};

		const expectedResponse: AuthResponse = {
			title: 'Environment variable SSO_SECRET could not be found',
			crsid: mockUsers.me.crsid,
		};

		await authController.identify(mockAuth.authorisedRequest.me, mockResponse);

		expect(mockResponse.status).toHaveBeenCalledWith(500);
		expect(mockResponse.status).toHaveBeenCalledTimes(1);
		expect(mockResponse.send).toHaveBeenCalledWith(expectedResponse);
	});
});
