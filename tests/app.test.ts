import supertest from 'supertest';
import { mockSSO, mockQueries } from './mockData';
import app from '../src/app';

beforeAll(() => {
	process.env.BASE_URL = 'http://localhost:3000';
	process.env.SESSION_KEY = 'mock_session_key';
	process.env.SSO_SECRET = mockSSO.secret;
	delete process.env.LOG_PATH;
	delete process.env.SENTRY_DSN;
});

afterAll(() => {
	delete process.env.BASE_URL;
	delete process.env.SESSION_KEY;
	delete process.env.SSO_SECRET;
});

const request = supertest(app);
const RAVEN_AUTH_URL = 'https://raven.cam.ac.uk/auth/authenticate.html';

describe('Test the root path', () => {
	test('Should always respond with 200', async () => {
		const response = await request.get('/');
		expect(response.status).toBe(200);
	});
});

describe('Test the /validate endpoint (tests all SSOController functionality)', () => {
	test('Valid query: should respond with 200 and correct data', async () => {
		const query = mockQueries.login.valid;
		const response = await request.get(`/validate/?${query}`);

		expect(response.status).toBe(200);
		expect(response.body).toEqual({
			title: 'SSO signature valid',
			nonce: mockSSO.nonce,
		});
	});

	test('Valid query with wrong signature: should respond with 401 and correct title', async () => {
		const query = mockQueries.login.invalidSignature;
		const response = await request.get(`/validate/?${query}`);

		expect(response.status).toBe(401);
		expect(response.body).toEqual({
			title: 'SSO signature invalid',
		});
	});

	test('Invalid query: should respond with 400 and correct title', async () => {
		const query = mockQueries.login.invalidPayload;
		const response = await request.get(`/validate/?${query}`);

		expect(response.status).toBe(400);
		expect(response.body).toEqual({
			title: 'SSO payload invalid',
		});
	});

	test('No query: should respond with 400 and correct title', async () => {
		const response = await request.get('/validate');

		expect(response.status).toBe(400);
		expect(response.body).toEqual({
			title: 'SSO payload not found',
		});
	});
});

describe('Test the /login endpoint', () => {
	test('Should always respond with 302 and redirect to Raven', async () => {
		const response = await request.get('/login');
		expect(response.status).toBe(302);
		expect(response.header.location).toContain(RAVEN_AUTH_URL);
	});
});

describe('Test the /results endpoint', () => {
	test('Should always respond with 200', async () => {
		const response = await request.get('/results');
		expect(response.status).toBe(200);
	});
});

describe('Test the /identify endpoint', () => {
	test('No query: should respond with 401 (in-depth testing done in AuthControllers.test)', async () => {
		const response = await request.get('/identify');
		expect(response.status).toBe(401);
	});
});
