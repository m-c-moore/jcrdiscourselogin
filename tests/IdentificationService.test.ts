import { mockUsers, mockAuth } from './mockData';
import findDowningStudent from '../src/services/IdentificationService';

beforeAll(() => {
	process.env.CSV_PATH = mockAuth.csvPath;
	delete process.env.LOG_PATH;
	delete process.env.SENTRY_DSN;
});

afterAll(() => {
	delete process.env.CSV_PATH;
});

describe('Test the IdentificationService', () => {
	test('findNameFromCRSid() should return a name given a Downing CRSid', async () => {
		const foundStudent = await findDowningStudent(mockUsers.me.crsid);
		expect(foundStudent).toEqual({
			name: mockUsers.me.name,
			crsid: mockUsers.me.crsid,
			admin: mockUsers.me.admin,
			moderator: mockUsers.me.moderator,
			addGroups: undefined,
			roles: mockUsers.me.roles,
		});
	});

	test('findNameFromCRSid() should return undefined given anything else', async () => {
		const foundStudent = await findDowningStudent(mockUsers.fake.crsid);
		expect(foundStudent).toEqual(undefined);
	});
});
