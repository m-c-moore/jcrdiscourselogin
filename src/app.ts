import express from 'express';
import passport from 'passport';
import RavenStrategy from 'passport-raven';
import session from '@authentication/cookie-session';
import * as Sentry from '@sentry/node';
import dotenv from 'dotenv';

import AuthController from './controllers/AuthController';
import SSOController from './controllers/SSOController';
import VoteController from './controllers/VoteController';

dotenv.config();
const { BASE_URL, SESSION_KEY, SENTRY_DSN } = process.env;

Sentry.init({ dsn: SENTRY_DSN });

if (!BASE_URL) {
	throw new Error('Environment variable BASE_URL could not be found');
}
if (!SESSION_KEY) {
	throw new Error('Environment variable SESSION_KEY could not be found');
}

const app = express();

app.use(
	session({
		maxAge: '1 minute',
		keys: [SESSION_KEY],
	})
);

passport.use(
	new RavenStrategy(
		{
			audience: BASE_URL,
			desc: 'Login page for the Downing JCR Discourse',
			msg: 'they need to check that you are a current Downing student',
			debug: false,
		},
		(crsid: string, _response: any, cb: (arg0: null, arg1: { crsid: string }) => void): void => {
			cb(null, { crsid });
		}
	)
);

passport.serializeUser((user: { crsid: string }, done: (arg0: null, user?: unknown) => void) => {
	done(null, user);
});

passport.deserializeUser((user: { crsid: string }, done: (arg0: null, user?: unknown) => void) => {
	done(null, user);
});

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static('views'));
app.use(express.static('public'));

const ssoController = new SSOController().router;
app.get('/validate', ssoController);

app.get('/login', passport.authenticate('raven'), (_req: any, res: any): void => {
	res.redirect('/results');
});

app.use('/results', express.static('views/results.html'));

const authController = new AuthController().router;
app.get('/identify', authController);

app.use('/locked', express.static('views/locked.html'));

const voteController = new VoteController().router;
app.get('/vote', voteController);

export default app;
