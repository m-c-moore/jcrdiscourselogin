import express from 'express';
import * as Sentry from '@sentry/node';
import logger from '../logger';
import VoteService from '../services/VoteService';

export default class VoteController {
	public readonly router: express.Router;
	private readonly voteService: VoteService;

	public constructor() {
		this.router = express.Router();
		this.router.get('/vote', this.submitVote.bind(this));
		this.voteService = new VoteService();
	}

	public submitVote(req: any, res: any): void {
		let statusCode: number;

		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'VoteController');
			scope.setExtra('function', 'vote');
			if (req.query) {
				scope.setUser({ username: req.query.crsid });
			}
		});

		const crsid: string = req.query.crsid;
		const choice: string = req.query.choice;
		const voteKey: string = req.query.voteKey;

		try {
			if (this.voteService.validateVoteKey(crsid, voteKey)) {
				this.voteService.saveVote(crsid, choice);
				statusCode = 200;
			} else {
				statusCode = 401;
				logger('VC-01').log('info', `Unauthorised voting attempt by ${req.query.crsid}`);
			}
		} catch (err) {
			logger('VC-02').log('error', err.message);
			Sentry.captureException(err);
			statusCode = 500;
		}
		res.status(statusCode);
		res.send();
	}
}
