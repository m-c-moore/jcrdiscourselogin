import express from 'express';
import * as Sentry from '@sentry/node';
import SSOService from '../services/SSOService';
import logger from '../logger';

export interface SSOResponse {
	title: string;
	nonce?: string | string[];
}

export default class SSOController {
	public readonly router: express.Router;
	private readonly ssoService: SSOService;

	public constructor() {
		this.router = express.Router();
		this.router.get('/validate', this.validate.bind(this));
		this.ssoService = new SSOService();
	}

	public validate(req: any, res: any): void {
		let statusCode: number;
		let ssoResponse: SSOResponse;

		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'SSOController');
			scope.setExtra('function', 'validate');
		});

		const payload: string = req.query.sso;
		const signature: string = req.query.sig;

		if (!payload) {
			statusCode = 400;
			ssoResponse = {
				title: 'SSO payload not found',
			};
		} else {
			let validPayload = false;
			try {
				validPayload = this.ssoService.validatePayload(payload, signature);
				if (validPayload) {
					try {
						statusCode = 200;
						ssoResponse = {
							title: 'SSO signature valid',
							nonce: this.ssoService.getNonce(payload),
						};
					} catch (err) {
						statusCode = 400;
						ssoResponse = {
							title: 'SSO payload invalid',
						};
						logger('SC-01').log('info', 'Request made with invalid SSO payload');

						Sentry.withScope((scope: Sentry.Scope): void => {
							scope.setLevel(Sentry.Severity.Log);
							Sentry.captureMessage('Request made with invalid SSO payload');
						});
					}
				} else {
					statusCode = 401;
					ssoResponse = {
						title: 'SSO signature invalid',
					};
					logger('SC-02').log('info', 'Request made with invalid SSO signature');

					Sentry.withScope((scope: Sentry.Scope): void => {
						scope.setLevel(Sentry.Severity.Log);
						Sentry.captureMessage('Request made with invalid SSO signature');
					});
				}
			} catch (err) {
				statusCode = 500;
				ssoResponse = {
					title: err.message,
				};
				logger('SC-03').log('error', err.message);
				Sentry.captureException(err);
			}
		}
		res.status(statusCode);
		res.send(ssoResponse);
	}
}
