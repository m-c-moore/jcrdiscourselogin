import express from 'express';
import * as Sentry from '@sentry/node';
import findDowningStudent, { DowningStudent } from '../services/IdentificationService';
import SSOService from '../services/SSOService';
import logger from '../logger';
import VoteService from '../services/VoteService';

export interface AuthResponse {
	title: string;
	crsid?: string;
	name?: string;
	redirectQuery?: string;
	voteKey?: string;
}

export default class AuthController {
	public readonly router: express.Router;
	private readonly ssoService: SSOService;

	public constructor() {
		this.router = express.Router();
		this.router.get('/identify', this.identify.bind(this));
		this.ssoService = new SSOService();
	}

	public async identify(req: any, res: any): Promise<void> {
		let statusCode: number;
		let authResponse: AuthResponse;

		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'AuthController');
			scope.setExtra('function', 'identify');
			if (req.user) {
				scope.setUser({ username: req.user.crsid });
			}
		});

		try {
			if (!req.isAuthenticated() || !req.user) {
				statusCode = 401;
				authResponse = {
					title: 'Unauthorised',
				};
				logger('AC-01').log(
					'info',
					`Unauthorised login attempt ${req.user ? `by ${req.user.crsid}` : '- no user found'}`
				);
			} else {
				const foundUser: DowningStudent | undefined = await findDowningStudent(req.user.crsid);

				if (!foundUser || foundUser.crsid !== req.user.crsid) {
					statusCode = 403;
					authResponse = {
						title: 'Forbidden',
						crsid: req.user.crsid,
					};
					logger('AC-02').log('info', `Forbidden login attempt by ${req.user.crsid}`);

					Sentry.withScope((scope: Sentry.Scope): void => {
						scope.setLevel(Sentry.Severity.Log);
						Sentry.captureMessage('Forbidden login attempt');
					});
				} else if (process.env.LOCKED) {
					statusCode = 423;
					const voteService = new VoteService();
					authResponse = {
						title: 'Locked',
						crsid: req.user.crsid,
						voteKey: voteService.generateVoteKey(req.user.crsid),
					};
					logger('AC-05').log('info', `Locked login attempt by ${req.user.crsid}`);

					Sentry.withScope((scope: Sentry.Scope): void => {
						scope.setLevel(Sentry.Severity.Log);
						Sentry.captureMessage('Forbidden login attempt');
					});
				} else {
					({ statusCode, authResponse } = this.generateRedirectQuery(req.query.nonce, foundUser));
				}
			}
		} catch (err) {
			statusCode = 500;
			authResponse = {
				title: err.message,
			};
			logger('AC-03').log('warn', err.message);
			Sentry.captureException(err);
		}
		res.status(statusCode);
		res.send(authResponse);
	}

	public generateRedirectQuery(
		nonce: string,
		foundUser: DowningStudent
	): {
		statusCode: number;
		authResponse: AuthResponse;
	} {
		const { JCR_COMMS_CRSID } = process.env;

		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'AuthController');
			scope.setExtra('function', 'generateRedirectQuery');
		});

		if (foundUser.crsid === JCR_COMMS_CRSID) {
			return this.generateCommsRedirectQuery(nonce, foundUser.crsid);
		}

		return this.generateNormalRedirectQuery(nonce, foundUser);
	}

	private generateNormalRedirectQuery(
		nonce: string,
		foundUser: DowningStudent
	): {
		statusCode: number;
		authResponse: AuthResponse;
	} {
		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.setUser({ username: foundUser.crsid });
		});

		let statusCode: number;
		let authResponse: AuthResponse;

		const parameters = {
			nonce,
			email: `${foundUser.crsid}@cam.ac.uk`,
			external_id: foundUser.crsid,
			username: foundUser.crsid,
			name: foundUser.name,
		};

		if (foundUser.admin) {
			Object.assign(parameters, { admin: foundUser.admin });
		}
		if (foundUser.moderator) {
			Object.assign(parameters, { moderator: foundUser.moderator });
		}
		if (foundUser.addGroups) {
			Object.assign(parameters, { add_groups: foundUser.addGroups.join(',') });
		}
		Object.assign(parameters, { roles: foundUser.roles.join(', ') });

		try {
			statusCode = 200;
			authResponse = {
				title: 'Successfully generated query string',
				name: foundUser.name,
				redirectQuery: this.ssoService.buildLoginString(parameters, true),
			};
		} catch (err) {
			statusCode = 500;
			authResponse = {
				title: err.message,
				crsid: foundUser.crsid,
			};
			logger('AC-04').log('warn', `${foundUser.crsid} - ${err.message}`);
			Sentry.captureException(err);
		}

		return { statusCode, authResponse };
	}

	private generateCommsRedirectQuery(
		nonce: string,
		crsid: string
	): {
		statusCode: number;
		authResponse: AuthResponse;
	} {
		const { JCR_COMMS_EMAIL, JCR_COMMS_USERNAME } = process.env;

		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.setUser({ username: `${JCR_COMMS_USERNAME} (${crsid})` });
		});

		let statusCode: number;
		let authResponse: AuthResponse;

		const parameters = {
			nonce,
			email: JCR_COMMS_EMAIL,
			external_id: JCR_COMMS_USERNAME,
			username: JCR_COMMS_USERNAME,
			name: 'JCR Comms',
			admin: 'true',
			moderator: 'true',
		};

		try {
			statusCode = 200;
			authResponse = {
				title: 'Successfully generated query string',
				name: `${crsid} (as ${JCR_COMMS_USERNAME})`,
				redirectQuery: this.ssoService.buildLoginString(parameters, false),
			};
		} catch (err) {
			statusCode = 500;
			authResponse = {
				title: err.message,
				crsid: `${crsid} (as ${JCR_COMMS_USERNAME})`,
			};
			logger('AC-04').log('warn', `${crsid} (as ${JCR_COMMS_USERNAME}) - ${err.message}`);
			Sentry.captureException(err);
		}

		return { statusCode, authResponse };
	}
}
