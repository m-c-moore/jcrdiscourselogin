import winston from 'winston';
import dotenv from 'dotenv';

dotenv.config();
const { LOG_PATH, SHOW_LOGS } = process.env;

const options = {
	file: {
		level: 'info',
		filename: LOG_PATH,
		handleExceptions: true,
		json: true,
		maxsize: 5242880, // 5MB
		maxFiles: 5,
		colorize: false,
		timestamp: (): number => Date.now(),
	},
	console: {
		level: 'info',
		handleExceptions: true,
		json: false,
		colorize: true,
	},
};

const transports: any[] = [];

if (LOG_PATH) {
	transports.push(new winston.transports.File(options.file));
}
if (SHOW_LOGS === 'true') {
	transports.push(new winston.transports.Console(options.console));
}

const myFormat = winston.format.printf(
	({ level, message, label, timestamp }: any) => `${timestamp} [${label}] ${level}: ${message}`
);

export default function logger(label: string): winston.Logger {
	return winston.createLogger({
		format: winston.format.combine(winston.format.label({ label }), winston.format.timestamp(), myFormat),
		transports,
		exitOnError: false,
	});
}
