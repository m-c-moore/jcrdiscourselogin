import winston from 'winston';
import crypto, { Hmac } from 'crypto';
import * as Sentry from '@sentry/node';
import logger from '../logger';

export default class VoteService {
	private voteLogger(): winston.Logger {
		const options = {
			file: {
				level: 'info',
				filename: process.env.VOTE_PATH,
				handleExceptions: true,
				json: true,
				maxsize: 5242880, // 5MB
				maxFiles: 5,
				colorize: false,
				timestamp: (): number => Date.now(),
			},
		};

		const transports: any[] = [new winston.transports.File(options.file)];
		const myFormat = winston.format.printf(({ message, timestamp }: any) => `${timestamp} ${message}`);
		return winston.createLogger({
			format: winston.format.combine(winston.format.timestamp(), myFormat),
			transports,
			exitOnError: false,
		});
	}

	public saveVote(crsid: string, choice: string): void {
		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'VotingService');
			scope.setExtra('function', 'saveVote');
			scope.setUser({ username: crsid });
		});

		try {
			this.voteLogger().log('info', `${crsid} - ${choice}`);
			Sentry.withScope((scope: Sentry.Scope): void => {
				scope.setLevel(Sentry.Severity.Log);
				Sentry.captureMessage(`Vote for ${choice}`);
			});
		} catch (err) {
			logger('VS-02').log('error', err.message);
			Sentry.captureException(err);
			throw new Error(`Error writing vote by "${crsid} - ${choice}"`);
		}
	}

	public generateVoteKey(crsid: string): string {
		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'VoteService');
			scope.setExtra('function', 'generateVoteKey');
		});

		const { VOTE_SECRET } = process.env;

		if (!VOTE_SECRET) {
			const err = new Error('Environment variable VOTE_SECRET could not be found');

			logger('VS-02').log('error', err.message);
			Sentry.withScope((scope: Sentry.Scope): void => {
				scope.setLevel(Sentry.Severity.Fatal);
				Sentry.captureException(err);
			});

			throw err;
		}

		const hmac: Hmac = crypto.createHmac('sha256', crsid + VOTE_SECRET);
		hmac.update(crsid);
		return hmac.digest('hex');
	}

	public validateVoteKey(crsid: string, voteKey: string): boolean {
		return this.generateVoteKey(crsid) === voteKey;
	}
}
