import crypto, { Hmac } from 'crypto';
import querystring, { ParsedUrlQueryInput } from 'querystring';
import * as Sentry from '@sentry/node';
import logger from '../logger';

export default class SSOService {
	private getHmac(): Hmac {
		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'SSOService');
			scope.setExtra('function', 'getHmac');
		});

		const secret = process.env.SSO_SECRET;

		if (!secret) {
			const err = new Error('Environment variable SSO_SECRET could not be found');

			logger('SS-01').log('error', err.message);
			Sentry.withScope((scope: Sentry.Scope): void => {
				scope.setLevel(Sentry.Severity.Fatal);
				Sentry.captureException(err);
			});

			throw err;
		}

		return crypto.createHmac('sha256', secret);
	}

	public validatePayload(payload: string, signature: string): boolean {
		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'SSOService');
			scope.setExtra('function', 'validatePayload');
		});

		const hmac: Hmac = this.getHmac();
		hmac.update(querystring.unescape(payload));
		const hmacSignature = hmac.digest('hex');

		return hmacSignature === signature;
	}

	public getNonce(payload: string): string | string[] {
		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'SSOService');
			scope.setExtra('function', 'getNonce');
			scope.setLevel(Sentry.Severity.Warning);
		});

		const query = querystring.parse(Buffer.from(querystring.unescape(payload), 'base64').toString());

		if ('nonce' in query) {
			return query.nonce;
		}

		throw new Error('No nonce in payload');
	}

	private validateQueryParameters(parameters: ParsedUrlQueryInput): void {
		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.setExtra('function', 'validateQueryParameter');
		});

		const expectedParameters = [
			{ name: 'nonce', regexPattern: /.+/ },
			{ name: 'email', regexPattern: /^[a-z]+[0-9]+@cam\.ac\.uk$/ },
			{ name: 'username', regexPattern: /^[a-z]+[0-9]+$/ },
			{ name: 'admin', regexPattern: /^(true|false)$/, optional: true },
			{ name: 'moderator', regexPattern: /^(true|false)$/, optional: true },
			{ name: 'add_groups', regexPattern: /^[a-zA-Z0-9_,]+$/, optional: true },
			{ name: 'roles', regexPattern: /^$|^.+$/ },
		];

		expectedParameters.forEach(
			(expectedParameter: { name: string; regexPattern: RegExp; optional?: boolean }): void => {
				const receivedParameter = parameters[expectedParameter.name];
				let errorMessage: string | undefined;

				if (receivedParameter === undefined && !expectedParameter.optional) {
					errorMessage = `Missing required parameter "${expectedParameter.name}"`;
				} else if (receivedParameter && !expectedParameter.regexPattern.exec(String(receivedParameter))) {
					errorMessage = `"${receivedParameter}" is not a valid value for the parameter "${expectedParameter.name}"`;
				}

				if (errorMessage) {
					logger('SS-02').log('warn', errorMessage);
					throw new Error(errorMessage);
				}
			}
		);

		if (parameters.external_id !== parameters.username) {
			const errorMessage = `External id "${parameters.external_id}" does not match username "${parameters.username}"`;
			logger('SS-03').log('warn', errorMessage);
			throw new Error(errorMessage);
		}

		if (parameters.external_id !== String(parameters.email).split('@')[0]) {
			const errorMessage = `External id "${parameters.external_id}" does not match start of email "${parameters.email}"`;
			logger('SS-04').log('warn', errorMessage);
			throw new Error(errorMessage);
		}
	}

	public buildLoginString(parameters: ParsedUrlQueryInput, validateParameters: boolean): string {
		Sentry.configureScope((scope: Sentry.Scope): void => {
			scope.clear();
			scope.setExtra('file', 'SSOService');
			scope.setExtra('function', 'getNonce');
			scope.setLevel(Sentry.Severity.Warning);
			if (parameters.username) {
				scope.setUser({ username: String(parameters.username) });
			}
		});

		if (validateParameters) {
			this.validateQueryParameters(parameters);
		}

		let queryString = querystring.stringify(parameters);
		queryString = queryString.replace('roles', 'custom.user_field_1');

		const payload = Buffer.from(queryString, 'utf-8').toString('base64');
		const hmac = this.getHmac();
		hmac.update(payload);

		return querystring.stringify({
			sso: payload,
			sig: hmac.digest('hex'),
		});
	}
}
