import csv from 'csv-parser';
import fs from 'fs';
import * as Sentry from '@sentry/node';
import logger from '../logger';

interface CSVRow {
	crsid: string;
	name: string;
	admin?: string;
	moderator?: string;
	addGroups?: string;
	roles?: string;
}

export interface DowningStudent {
	crsid: string;
	name: string;
	admin?: string;
	moderator?: string;
	addGroups?: string[];
	roles: string[];
}

export default async function findDowningStudent(crsid: string): Promise<DowningStudent | undefined> {
	const csvPath = process.env.CSV_PATH;

	Sentry.configureScope((scope: Sentry.Scope): void => {
		scope.clear();
		scope.setExtra('file', 'IdentificationService');
		scope.setExtra('function', 'findDowningStudent');
		scope.setUser({ username: crsid });
	});

	if (!csvPath) {
		const err = new Error('Environment variable CSV_PATH could not be found');

		logger('IS-01').log('error', err.message);
		Sentry.withScope((scope: Sentry.Scope): void => {
			scope.setLevel(Sentry.Severity.Fatal);
			Sentry.captureException(err);
		});

		throw err;
	}

	let readStream: fs.ReadStream;

	try {
		readStream = fs.createReadStream(csvPath);
	} catch (err) {
		logger('IS-02').log('error', err.message);
		Sentry.captureException(err);
		throw new Error('Error reading from Downing Students CSV');
	}

	return new Promise((res: (value: DowningStudent | undefined) => void, rej: (reason: Error) => void): void => {
		readStream
			.pipe(csv({ separator: ',' }))

			.on('data', (row: CSVRow) => {
				if (row.crsid === crsid) {
					const foundStudent: DowningStudent = {
						crsid: row.crsid,
						name: row.name,
						admin: row.admin ? row.admin : undefined,
						moderator: row.moderator ? row.moderator : undefined,
						addGroups: row.addGroups ? row.addGroups.split(';') : undefined,
						roles: row.roles ? row.roles.split(';') : [''],
					};
					res(foundStudent);
				}
			})

			.on('error', (err: Error) => {
				logger('IS-03').log('warn', err.message);
				Sentry.captureException(err);
				rej(err);
			})

			.on('end', () => {
				res(undefined);
			});
	});
}
