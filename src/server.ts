import dotenv from 'dotenv';
import app from './app';

dotenv.config();

const PORT = Number(process.env.PORT);

if (!PORT) {
	throw new Error('Environment variable PORT could not be found');
}

app.listen(PORT, () => console.log(`App listening on port ${PORT}`));
