/* eslint @typescript-eslint/no-non-null-assertion: 0 */

function submitVote(choice: string): void {
	const xhttp = new XMLHttpRequest();
	const params = sessionStorage.getItem('vote_params');

	xhttp.open('GET', `/vote?${params}&choice=${choice}`, true);

	xhttp.onload = (): void => {
		if (xhttp.status === 200) {
			document.getElementById('vote-status')!.innerHTML =
				'Thanks! Your vote has been recorded. Feel free to vote again, but only your most recent one will count.';
		} else if (xhttp.status === 401) {
			document.getElementById('vote-status')!.innerHTML = 'Oops! An error occurred while authorising your request.';
		} else {
			document.getElementById('vote-status')!.innerHTML = 'Oops! An error occurred, please try again.';
		}
	};

	xhttp.send();

	disableButtons();
}

function disableButtons(): void {
	const buttonIds = ['vote-keep', 'vote-dont', 'vote-lose'];

	buttonIds.forEach((id: string) => {
		const button = document.getElementById(id)!;
		button.setAttribute('disabled', '');
	});

	window.setTimeout(() => {
		buttonIds.forEach((id: string) => {
			const button = document.getElementById(id)!;
			button.removeAttribute('disabled');
		});
	}, 1500);
}

window.onload = function(): void {
	document.getElementById('vote-keep')!.addEventListener('click', () => submitVote('keep_it'));
	document.getElementById('vote-dont')!.addEventListener('click', () => submitVote('dont_mind'));
	document.getElementById('vote-lose')!.addEventListener('click', () => submitVote('lose_it'));
};
