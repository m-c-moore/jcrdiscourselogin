/* eslint @typescript-eslint/no-non-null-assertion: 0 */

export default function renderCard(title: string, subtitle: string, message: string, contact?: string): void {
	document.title = title;
	let resultsHTML = '';

	resultsHTML += '<div class="card"><div class="card-body">';
	resultsHTML += `<h4 class="card-title">${title}</h4>`;
	resultsHTML += `<h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>`;
	resultsHTML += `<p class="card-text">${message}</p>`;

	if (contact) {
		resultsHTML += `<p>${contact}, please contact the <a id="magenta_links" href="mailto:comms@jcr.dow.ac.uk">Downing Comms Officer</a> and they will try to help.</p >`;
	}

	resultsHTML += '</div ></div >';
	document.getElementById('card-placeholder')!.innerHTML = resultsHTML;
}
