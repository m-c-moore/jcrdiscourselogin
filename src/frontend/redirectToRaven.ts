import { renderValidationFailure } from './renderValidationResults';

export function checkXHTTPResponse(xhttp: XMLHttpRequest): void {
	const { status } = xhttp;
	const response = JSON.parse(xhttp.response);

	if (status === 200) {
		sessionStorage.setItem('discourse_nonce', response.nonce);
		window.location.replace('/login');
	} else {
		renderValidationFailure(status, response.title);
	}
}

export function validateSSO(): void {
	const xhttp = new XMLHttpRequest();
	const params = window.location.href.split('?')[1];

	xhttp.open('GET', `/validate/?${params}`, true);

	xhttp.onload = (): void => {
		checkXHTTPResponse(xhttp);
	};

	xhttp.send();
}

window.onload = validateSSO;
