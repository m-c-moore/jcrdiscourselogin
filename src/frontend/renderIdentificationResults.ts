import renderCard from './renderCard';

const discourseLoginURL = 'http://forum.downingjcr.co.uk/login';
const tryAgainMessage = `please <a id="magenta_links" href="${discourseLoginURL}">click here</a> to try logging in again.`;
const directAccessMessage = `This may have been because you tried to access this page directly rather than through the Downing JCR Forum. If that is the case, ${tryAgainMessage}`;

export function renderNoNonce(): void {
	const title = 'New session who dis?';
	const subtitle = "Sorry, we couldn't verify your login session.";
	const message = directAccessMessage;
	const contactReason = "If that isn't the case and the problem persists";

	renderCard(title, subtitle, message, contactReason);
}

export function renderIdentificationSuccess(name?: string): void {
	const title = name ? `Welcome, ${name}!` : 'Welcome!';
	const subtitle = '';
	const message = 'You will shortly be redirected back to the Downing JCR Forum. Please do not refresh the page.';

	renderCard(title, subtitle, message);
}

export function renderIdentificationFailure(
	responseStatus: number,
	responseTitle: string,
	responseCRSid?: string
): void {
	let title = `${responseStatus}: ${responseTitle}`;
	let subtitle = responseCRSid ? `Sorry ${responseCRSid}` : 'Sorry';
	let message = directAccessMessage;
	let contactReason = 'If the problem persists';

	switch (responseStatus) {
		case 400:
			subtitle += ', we had some trouble generating a query string from the data we received.';
			break;
		case 401:
			subtitle += ', Raven login appears to have failed, or the session has expired.';
			break;
		case 403:
			subtitle += ', our records show that you are not a current Downing undergraduate.';
			message = '';
			contactReason = 'If you believe this is a mistake';
			break;
		case 500:
			title = '500: Internal server error';
			subtitle += ', our server encountered an error when processing your request.';
			message = `P${tryAgainMessage.substring(1)}`;
			break;
		default:
			title = 'Oops!';
			subtitle = `An unknown error has occurred (${responseStatus}).`;
			message = `P${tryAgainMessage.substring(1)}`;
	}
	renderCard(title, subtitle, message, contactReason);
}
