import renderCard from './renderCard';

const discourseLoginURL = 'http://forum.downingjcr.co.uk/login';
const tryAgainMessage = `please <a id="magenta_links" href="${discourseLoginURL}">click here</a> to try logging in again.`;
const directAccessMessage = `This may have been because you tried to access this page directly rather than through the Downing JCR Forum. If that is the case, ${tryAgainMessage}`;

export function renderValidationFailure(responseStatus: number, responseTitle: string): void {
	let title = `${responseStatus}: ${responseTitle}`;
	let subtitle: string;
	let message = `Please <a id="magenta_links" href="${discourseLoginURL}">click here</a> to try logging in again.`;

	switch (responseStatus) {
		case 400:
			if (responseTitle === 'SSO payload not found') {
				subtitle = 'Sorry, we could not find an SSO payload in your request.';
				message = directAccessMessage;
			} else {
				subtitle = 'Sorry, an error occurred when decoding the SSO payload from Discourse.';
			}
			break;
		case 401:
			subtitle = 'Sorry, the signature of the SSO payload from Discourse could not be verified.';
			break;
		case 500:
			title = '500: Internal server error';
			subtitle = 'Sorry, our server encountered an error while processing your request.';
			break;
		default:
			title = 'Oops!';
			subtitle = `An unknown error has occurred (${responseStatus}).`;
	}
	const contactReason = 'If the problem persists';
	renderCard(title, subtitle, message, contactReason);
}
