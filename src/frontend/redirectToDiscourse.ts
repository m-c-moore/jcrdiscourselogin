import { renderNoNonce, renderIdentificationSuccess, renderIdentificationFailure } from './renderIdentificationResults';

const discourseSessionURL = 'http://forum.downingjcr.co.uk/session/sso_login';

export function checkXHTTPResponse(xhttp: XMLHttpRequest): void {
	const { status } = xhttp;
	const response = JSON.parse(xhttp.response);

	if (status === 200) {
		renderIdentificationSuccess(response.name);
		window.sessionStorage.removeItem('discourse_nonce');
		window.location.replace(`${discourseSessionURL}/?${response.redirectQuery}`);
	} else if (status === 423) {
		window.location.replace(`/locked`);
		sessionStorage.setItem('vote_params', `crsid=${response.crsid}&voteKey=${response.voteKey}`);
	} else {
		renderIdentificationFailure(status, response.title, response.crsid);
	}
}

export function identifyUser(): void {
	const xhttp = new XMLHttpRequest();
	const nonce = sessionStorage.getItem('discourse_nonce');

	if (!nonce) {
		renderNoNonce();
		return;
	}

	xhttp.open('GET', `/identify/?nonce=${nonce}`, true);

	xhttp.onload = (): void => {
		checkXHTTPResponse(xhttp);
	};

	xhttp.send();
}

window.onload = identifyUser;
