#!/bin/sh -v
set -e
cd /var/www/

echo "===== Copy .env, downingStudents.csv and logs to temp server ====="
cp login.forum.downingjcr.co.uk/.env login.forum.downingjcr.co.uk.temp/
cp login.forum.downingjcr.co.uk/downingStudents.csv login.forum.downingjcr.co.uk.temp/
cp -r login.forum.downingjcr.co.uk/logs login.forum.downingjcr.co.uk.temp/

echo "===== Copy node modules to temp server ====="
if [ -e login.forum.downingjcr.co.uk/node_modules ]; then
	cp -r login.forum.downingjcr.co.uk/node_modules login.forum.downingjcr.co.uk.temp/
fi

echo "===== Install node modules in temp server ====="
sudo chown -R $USER:$GROUP ~/.npm 2> /dev/null || echo "xxxxx Failed to change ownership of ~/.npm (this may need to be done manually) xxxxx"
sudo chown -R $USER:$GROUP ~/.config 2> /dev/null || echo "xxxxx Failed to change ownership of ~/.config (this may need to be done manually) xxxxx"
(cd login.forum.downingjcr.co.uk.temp/ && npm install --only=prod --loglevel=error)

echo "===== Kill the current server ====="
kill $(lsof -t -i:8905) || echo "xxxxx No process running on port 8905 xxxxx"

echo "===== Replace current server with the temp one ====="
mv login.forum.downingjcr.co.uk login.forum.downingjcr.co.uk.old
mv login.forum.downingjcr.co.uk.temp login.forum.downingjcr.co.uk

echo "===== Remove the old server in the background ====="
rm -r login.forum.downingjcr.co.uk.old &

echo "===== Start the new server =====" 
cd login.forum.downingjcr.co.uk/
node dist/server.js &

exit 0
